require './planetwars.rb'

  

class Helpers
  def initialize pw
    @pw = pw
  end

  def log txt
    File.open('./mylogs.txt', 'a') {|file| file.write(txt.to_s+"\n")}
  end

  def predict_num_ships source, dest
    (dest.num_ships + (dest.growth_rate * (@pw.distance(source.planet_id, dest.planet_id)))).ceil+5
  end

  def fleet_to? planet, fleets
    fleets.any? {|f| f.destination_planet == planet.planet_id}
  end

  def fleet_from? planet, fleets
    fleets.any? {|f| f.source_planet == planet.planet_id}
  end

  def weakest planets
    planets.max_by {|planet| 0 - planet.num_ships}
  end

  def strongest planets
    planets.max_by {|planet| planet.num_ships}
  end

  def strongest_planets planets, limit
    planets.sort_by {|pl| 0 - pl.num_ships}.take(limit)
  end

  def weakest_planets planets, limit
    planets.sort_by {|pl| pl.num_ships}.take(limit)
  end

  def weakest_attackable planets
    planets.sort_by {|pl| pl.num_ships}.select {|pl| !fleet_to?(pl, @pw.my_fleets)}.first
  end

  def planets_needed_to_attack target, planets
    return [] if !target || !planets
    num = 1
    planets_needed = []
    planets = strongest_planets(planets, planets.count)
    loop do
      furthest_planet = planets.take(num).sort_by {|pl| 0 - @pw.distance(pl.planet_id, target.planet_id)}.first
      break if !furthest_planet
      target_num_ships = target.owner == 0 ? target.num_ships : predict_num_ships(furthest_planet, target)
      if planets.take(num).reduce(0) {|ships, pl| ships+pl.num_ships} > target_num_ships
        planets_needed = planets.take(num)
        break
      else
        num += 1
        break if num > planets.count
      end
    end
    planets_needed
  end

  def cluster_attack target, planets
    planets = strongest_planets(planets, planets.count)
    planets.each do |pl|
      @pw.issue_order(pl.planet_id, target.planet_id, pl.num_ships)
    end
  end
end






# get any number of my planets that can cluster attack weakest and cluster attack


def do_turn(pw)
  @helpers = Helpers.new(pw)
  

  weakest = @helpers.weakest_attackable(pw.enemy_planets + pw.neutral_planets)
  planets = @helpers.planets_needed_to_attack(weakest, pw.my_planets)
  if weakest && !planets.empty?
    @helpers.cluster_attack(weakest, planets)
  end









  # (1) don't do anything if we have a fleet in flight
#   return if pw.my_fleets.length >= 1

#   # (2) find my strongest planet
#   source = -1
#   source_score = -999999.0
#   source_num_ships = 0
#   pw.my_planets.each do |planet|
#     score = planet.num_ships.to_f
#     if score > source_score
#       source_score = score
#       source = planet.planet_id
#       source_num_ships = planet.num_ships
#     end
#   end

#   # (3) find the weakest planet belonging that isn't mine
#   dest = -1
#   dest_score = -999999.0
#   pw.not_my_planets.each do |p|
#     score = 1.0 / (1 + p.num_ships)
#     if score > dest_score
#       dest_score = score
#       dest = p.planet_id
#     end
#   end

#   # (4) send half of the ships from my strongest planet to the weakest one that i do not own
#   if source >= 0 and dest >= 0
#     num_ships = source_num_ships / 2
#     pw.issue_order(source, dest, num_ships)
#   end
end

map_data = ''
loop do
  current_line = gets.strip
  if current_line.length >= 2 and current_line[0..1] == "go"
    pw = PlanetWars.new(map_data)
    do_turn(pw)
    pw.finish_turn
    map_data = ''
  else
    map_data += current_line + "\n"
  end


end
